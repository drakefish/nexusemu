#pragma once

namespace NexusEmu {
namespace Utils {

	class BitStreamWriter {
	public:
		BitStreamWriter() {}
		BitStreamWriter(std::vector<uint8_t>& buffer) :
			m_buffer(buffer) {}

		std::vector<uint8_t>& getBuffer() {
			return m_buffer;
		}

		// Flushe the current partial byte to the buffer if it's not empty.
		void FlushByte() {
			if (m_bitsInPartialByte != 0)
				FlushPartialByte();
		}

		template<typename T>
		void WriteInteger(T value, uint32_t bits = sizeof(T)*8) {
			InternalWrite<T>(value, bits);
		}

		template<>
		void WriteInteger<bool>(bool value, uint32_t bits) {
			InternalWrite<uint32_t>(value ? 1 : 0, bits);
		}

		void WriteFloat(float value) {
			uint32_t i = *reinterpret_cast<uint32_t*>(&value);
			InternalWrite<uint32_t>(i, sizeof(float) * 8);
		}

		void WriteString(std::wstring& str) {
			if (str.length() > 32767) {
				throw std::exception("Attempting to write a bigger string than the maximum length bitsize.");
			}

			bool bigLength = str.length() > 127;
			WriteInteger(bigLength, 1);
			WriteInteger(str.length(), bigLength ? 15 : 7);
			for (auto itr = str.begin(); itr != str.end(); itr++) {
				WriteInteger(*itr, 16);
			}
		}

		void WriteAnsiString(std::string& str) {
			if (str.length() > 32767) {
				throw std::exception("Attempting to write a bigger string than the maximum length bitsize.");
			}

			bool bigLength = str.length() > 127;
			WriteInteger(bigLength, 1);
			WriteInteger(str.length(), bigLength ? 15 : 7);
			for (auto itr = str.begin(); itr != str.end(); itr++) {
				WriteInteger(*itr, 16);
			}
		}

	private:
		template<typename T>
		void InternalWrite(T value, uint32_t bits) {
			static_assert(std::numeric_limits<T>::is_integer, "T must be an integer.");

			if (bits > sizeof(T) * BYTE_SIZE) {
				throw std::exception("Requested number of bits exceeds the type's bitsize.");
			}

			while (bits > 0)
			{
				uint32_t remainingBits = 8 - m_bitsInPartialByte;
				if (remainingBits > bits)
					remainingBits = bits;
				m_partialByte |= (value & ((1 << remainingBits) - 1)) << m_bitsInPartialByte;
				value >>= remainingBits;
				bits -= remainingBits;
				m_bitsInPartialByte = (remainingBits + m_bitsInPartialByte) & 7;

				if (m_bitsInPartialByte == 0)
					FlushPartialByte();
			}
		}

		// Flush the current partial byte to the buffer.
		void FlushPartialByte() {
			if (m_wpos >= m_buffer.max_size()) {
				throw std::exception("The maximum size of the underlying buffer has been reached.");
			}

			m_buffer.resize(m_buffer.size() + 1);
			m_buffer[m_wpos] = m_partialByte;
			m_bitsInPartialByte = 0;
			m_partialByte = 0;
			m_wpos++;
		}

		

	private:
		static const int BYTE_SIZE = 8;
		std::vector<uint8_t> m_buffer;
		uint32_t m_wpos;
		uint8_t m_partialByte;
		int32_t m_bitsInPartialByte;
	};

}
}