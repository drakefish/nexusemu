#include "stdafx.h"
#include "TcpConnection.h"


namespace NexusEmu {
namespace Network {

	void TcpConnection::Disconnect() {
		bool connected = true;
		m_connected.compare_exchange_strong(connected, false);

		if (!connected) {
			// we aren't connected, ignore this call.
			return;
		}

		if (m_socket.is_open()) {
			boost::system::error_code error;
			m_socket.cancel(error);
			m_socket.shutdown(tcp::socket::shutdown_both, error);
			m_socket.close(error);
		}

		std::lock_guard<std::mutex> g(m_writeQueueLock);

		std::swap(m_writeQueue, std::queue<std::vector<uint8_t>>());
		//m_disconnecting = false;
	}

	void TcpConnection::AsyncSend(std::vector<uint8_t>& buffer) {
		std::lock_guard<std::mutex> lock(m_writeQueueLock);

		bool wasEmpty = m_writeQueue.empty();
		m_writeQueue.push(buffer);
		if (wasEmpty) {
			AsyncWriteQueueFront();
		}
	}

	void TcpConnection::AsyncWriteQueueFront() {
		std::cout << "Writing queue front." << std::endl;
		boost::asio::async_write(getSocket(), boost::asio::buffer(m_writeQueue.front().data(), m_writeQueue.front().size()),
			std::bind(&TcpConnection::HandleWrite, getThis<TcpConnection>(),
			std::placeholders::_1, std::placeholders::_2));
	}

	void TcpConnection::HandleWrite(const boost::system::error_code& error, size_t bytes_transferred)  {
		if (error) {
			std::cerr << "handle_write() error: " << error.message();

			if (error != boost::asio::error::try_again &&
				error != boost::asio::error::would_block)
			{
				Disconnect();
			}
		}

		{
			std::lock_guard<std::mutex> g(m_writeQueueLock);

			// if not tryagain or wouldblock, pop front packet when we're done with it.
			if (!error && !m_writeQueue.empty()) {
				std::cout << "popped." << std::endl;
				m_writeQueue.pop();
			}

			if (!m_writeQueue.empty()) {
				// write next in queue.
				AsyncWriteQueueFront();
			}
			/*else if (m_disconnecting) {
			//shouldDisconnect = true;
			disconnect();
			}*/
		}

		/*
		if (shouldDisconnect) {
		disconnect();
		}
		*/

	}

}
}