#pragma once
#include "Network\GameServer.h"
#include "RealmConnection.h"

namespace NexusEmu {

	class RealmServer :
		public Network::GameServer 
	{
	public:
		RealmServer(boost::asio::io_service& io_service, tcp::endpoint& endpoint) :
			GameServer(io_service, endpoint) {}


	private:
		Network::TcpConnection::Pointer CreateConnection(boost::asio::io_service& iosvc) {
			return RealmConnection::Create(iosvc);
		}

		void HandleNewConnection(Network::TcpConnection::Pointer& conn) {
			std::cout << "RealmServer: New connection." << std::endl;
			conn->AsyncStart();
		}
	};

}