#pragma once
#include "TcpConnection.h"

namespace NexusEmu {
	namespace Network {

		class TcpServer {
		public:
			TcpServer(boost::asio::io_service& io_service, tcp::endpoint& endpoint) :
				m_acceptor(io_service, endpoint), m_service(io_service)  {}

			void Start() {
				AsyncAccept();
			}

			boost::asio::io_service& getIoService() {
				return m_service;
			}

		protected:
			virtual void HandleNewConnection(TcpConnection::Pointer& conn) = 0;
			virtual TcpConnection::Pointer CreateConnection(boost::asio::io_service& iosvc) = 0;

		private:
			void AsyncAccept() {
				TcpConnection::Pointer connection = CreateConnection(m_service);
				m_acceptor.async_accept(connection->getSocket(),
					std::bind(&TcpServer::HandleAccept, this, connection, std::placeholders::_1));
			}

			void HandleAccept(TcpConnection::Pointer& conn, const boost::system::error_code& error) {
				if (error) {
					std::cout << "HandleAccept() error: " << error.message() << std::endl;
					return;
				}

				HandleNewConnection(conn);
				AsyncAccept();
			}

		private:
			tcp::acceptor m_acceptor;
			boost::asio::io_service& m_service;
		};

	}
}