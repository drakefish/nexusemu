#pragma once
#include "Network\GameServer.h"
#include "AuthConnection.h"

namespace NexusEmu {

	class AuthServer :
		public Network::GameServer
	{
	public:
		AuthServer(boost::asio::io_service& io_service, tcp::endpoint& endpoint) :
			GameServer(io_service, endpoint) {}


	private:
		Network::TcpConnection::Pointer CreateConnection(boost::asio::io_service& iosvc) {
			return AuthConnection::Create(iosvc);
		}

		void HandleNewConnection(Network::TcpConnection::Pointer& conn) {
			std::cout << "AuthServer: New connection." << std::endl;
			conn->AsyncStart();
		}
	};

}