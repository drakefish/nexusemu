#pragma once
#include "Utils\DynamicObject.h"

using boost::asio::ip::tcp;

namespace NexusEmu {
namespace Network {
	//
	// Notes:
	//  TcpConnection handles the writing but child class handles reading.
	//   This is because the write queue is generic but reading really depends on the connection implementation.
	//

	class TcpConnection : public Utils::DynamicObject {
	public:
		typedef std::shared_ptr<TcpConnection> Pointer;

		tcp::socket& getSocket() {
			return m_socket;
		}

		void Disconnect();

		virtual void AsyncStart() = 0;

	protected:
		TcpConnection(boost::asio::io_service& iosvc) : 
			m_socket(iosvc), m_connected(true) { }

		void AsyncSend(std::vector<uint8_t>& buffer);

	private:
		void AsyncWriteQueueFront();
		void HandleWrite(const boost::system::error_code& error, size_t bytes_transferred);
		

	private:
		std::atomic<bool> m_connected;
		std::mutex m_writeQueueLock;
		std::queue<std::vector<uint8_t>> m_writeQueue;
		tcp::socket m_socket;
		std::vector<uint8_t> m_buffer;
	};

}
}