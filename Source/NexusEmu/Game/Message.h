#pragma once

namespace NexusEmu {
namespace Game{

	enum class Message {
		Ping1 = 0,
		Ping2 = 1,
		SHello = 2,
		CAuthRequest = 0x259,
		SAuthReply = 0x25D,
		CAuthPasswordDigest = 0x25C,
		SGatewayInfo = 0x198,
		SAuthSuccess = 0x25F,
		SRealmList = 0x35A,
		CSelectRealm = 0x372,
	};

}
}