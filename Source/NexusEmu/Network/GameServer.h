#pragma once
#include "Network\TcpServer.h"
#include "Network\GameConnection.h"

namespace NexusEmu {
namespace Network {

	class GameServer : 
		public TcpServer
	{
	public:
		GameServer(boost::asio::io_service& io_service, tcp::endpoint& endpoint) :
			TcpServer(io_service, endpoint) {}
		

	private:
		
	};

}
}