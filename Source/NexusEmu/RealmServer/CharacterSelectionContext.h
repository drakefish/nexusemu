#pragma once
#include "RealmConnection.h"

namespace NexusEmu {

	class CharacterSelectionContext {
	public:
		typedef std::shared_ptr<CharacterSelectionContext> Pointer;

		static Pointer Create() {
			return Pointer(new CharacterSelectionContext());
		}

		void Initialize(RealmConnection::Pointer& conn) {
			//conn->RegisterMessageHandler(Game::Message::CAuthRequest,
			//	std::bind(&CharacterSelectionContext::HandleAuthRequest, getThis<CharacterSelectionContext>(), std::placeholders::_1, std::placeholders::_2));
		}

	private:

	};

}