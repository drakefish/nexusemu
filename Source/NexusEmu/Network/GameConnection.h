#pragma once
#include "TcpConnection.h"
#include "Game\Message.h"
#include "Utils\BitStreamWriter.h"
#include "Utils\BitStreamReader.h"

namespace NexusEmu {
namespace Network {

	class GameConnection : 
		public TcpConnection 
	{

	public:
		typedef std::shared_ptr<GameConnection> Pointer;
		

		static Utils::BitStreamWriter CreatePacketWriter(Game::Message id);
		void SendPacket(Utils::BitStreamWriter& writer);
		
		

	protected:
		GameConnection(boost::asio::io_service& iosvc) :
			TcpConnection(iosvc) { }

		void AsyncReadHeader();
		void HandleReadHeader(const boost::system::error_code& error, size_t bytes_transferred);
		void HandleReadBody(Game::Message msgid, const boost::system::error_code& error, size_t bytes_transferred);

		virtual void HandleReceivedMessage(Game::Message, Utils::BitStreamReader&) = 0;

	private:
		//std::vector<uint8_t> m_buffer;
		Utils::BitStreamReader m_reader;
	};

}
}