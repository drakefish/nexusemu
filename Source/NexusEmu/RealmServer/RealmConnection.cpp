#include "stdafx.h"
#include "RealmConnection.h"
#include "CharacterSelectionContext.h"

namespace NexusEmu {

	void RealmConnection::AsyncStart() {
		//RealmContext::Create()->Initialize(getThis<RealmConnection>());
		CharacterSelectionContext::Create()->Initialize(getThis<RealmConnection>());

		AsyncReadHeader();

		Utils::BitStreamWriter writer = CreatePacketWriter(Game::Message::SHello);
		writer.WriteInteger(6072, 32);
		writer.WriteInteger(0, 32);
		writer.WriteInteger(0, 32);
		writer.WriteInteger(0ULL, 64);
		writer.WriteInteger(0, 16);
		writer.WriteInteger(3, 5);
		writer.WriteInteger(0x2229B228, 32);
		SendPacket(writer);
	}

	void RealmConnection::HandleReceivedMessage(Game::Message message, Utils::BitStreamReader& reader) {
		switch (message) {
		case Game::Message::Ping1:
		case Game::Message::Ping2:
		{
			auto writer = CreatePacketWriter(message);
			writer.WriteInteger<uint8_t>(reader.ReadInteger<uint8_t>(1), 1);
			SendPacket(writer);
			break;
		}
		default:
		{
			MessageHandler handler;

			{
				std::lock_guard<std::mutex> g(m_handlersLock);

				auto itr = m_handlers.find(message);
				if (itr == m_handlers.end()) {
					std::cout << "RealmServer Received unhandled message: " << static_cast<uint32_t>(message) << std::endl;
					break;
				}

				handler = itr->second;
			}

			handler(getThis<RealmConnection>(), reader);
			break;
		}
		}
	}
}

