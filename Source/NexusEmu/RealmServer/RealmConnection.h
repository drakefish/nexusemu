#pragma once
#include "Network\GameConnection.h"

namespace NexusEmu {

	class RealmConnection :
		public Network::GameConnection
	{
	public:
		typedef std::shared_ptr<RealmConnection> Pointer;
		typedef std::function<void(RealmConnection::Pointer&, Utils::BitStreamReader&)> MessageHandler;

		static Pointer Create(boost::asio::io_service& iosvc) {
			return Pointer(new RealmConnection(iosvc));
		}

		void AsyncStart();

		void UnregisterMessageHandler(Game::Message msg) {
			std::lock_guard<std::mutex> g(m_handlersLock);
			m_handlers.erase(msg);
		}

		void RegisterMessageHandler(Game::Message msg, MessageHandler handler) {
			std::lock_guard<std::mutex> g(m_handlersLock);
			m_handlers[msg] = handler;
		}

	private:
		RealmConnection(boost::asio::io_service& iosvc) :
			GameConnection(iosvc) { }

		void HandleReceivedMessage(Game::Message message, Utils::BitStreamReader& reader);

	private:
		std::mutex m_handlersLock;
		std::unordered_map<Game::Message, MessageHandler> m_handlers;
	};

}