#pragma once
#include <memory>

namespace NexusEmu {
namespace Utils {

	class DynamicObject : private std::enable_shared_from_this < DynamicObject > {
	public:
		DynamicObject() = default;

		template <class T>
		std::shared_ptr<T> getThis() {
			static_assert(std::is_base_of<DynamicObject, T>::value,
				"T must be a derivative of DynamicObject!");

			auto ptr = std::dynamic_pointer_cast<T>(shared_from_this());
			assert(ptr != nullptr);
			return std::move(ptr);
		}

		template <class T>
		std::shared_ptr<const T> getThis() const {
			static_assert(std::is_base_of<DynamicObject, T>::value,
				"T must be a derivative of DynamicObject!");

			auto ptr = std::dynamic_pointer_cast<const T>(shared_from_this());
			assert(ptr != nullptr);
			return std::move(ptr);
		}

		std::shared_ptr<DynamicObject> getThis() {
			return shared_from_this();
		}

		std::shared_ptr<const DynamicObject> getThis() const {
			return shared_from_this();
		}

	public:
		virtual ~DynamicObject() = default;

	private:
		DynamicObject(const DynamicObject&) = delete;
		DynamicObject& operator = (const DynamicObject&) = delete;
	};

}
}