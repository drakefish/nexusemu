#pragma once
#include "Utils\DynamicObject.h"
#include "AuthConnection.h"

namespace NexusEmu {
	
	class AuthContext :
		public Utils::DynamicObject 
	{
	public:
		typedef std::shared_ptr<AuthContext> Pointer;

		static Pointer Create() {
			return Pointer(new AuthContext());
		}

		void Initialize(AuthConnection::Pointer& conn) {
			conn->RegisterMessageHandler(Game::Message::CAuthRequest,
				std::bind(&AuthContext::HandleAuthRequest, getThis<AuthContext>(), std::placeholders::_1, std::placeholders::_2));
		}

	private:

		void HandleAuthRequest(AuthConnection::Pointer& conn, Utils::BitStreamReader& reader) {
			conn->UnregisterMessageHandler(Game::Message::CAuthRequest);

			auto accountName = reader.ReadString();
			auto buildNumber = reader.ReadInteger<uint32_t>();
			auto loginClientType = reader.ReadInteger<uint32_t>();

			std::wcout << "Client auth request: " << accountName
				<< "  build " << buildNumber 
				<< "  type " << loginClientType << std::endl;

			std::swap(accountName, m_accountName);

			conn->RegisterMessageHandler(Game::Message::CAuthPasswordDigest, 
				std::bind(&AuthContext::HandlePasswordDigest, getThis<AuthContext>(), std::placeholders::_1, std::placeholders::_2));

			// this was a test and it's not the right packet. We will have to dump fabian's authworker too.
			auto writer = conn->CreatePacketWriter(Game::Message::SAuthReply);
			writer.WriteInteger(1807548302ULL);
			writer.WriteInteger(18446744073407396780ULL);
			conn->SendPacket(writer);
		}

		void HandlePasswordDigest(AuthConnection::Pointer& conn, Utils::BitStreamReader& reader) {
			conn->UnregisterMessageHandler(Game::Message::CAuthPasswordDigest);
			
			// 20 bytes-long digest.

			conn->RegisterMessageHandler(Game::Message::CSelectRealm,
				std::bind(&AuthContext::HandleSelectRealm, getThis<AuthContext>(), std::placeholders::_1, std::placeholders::_2));


			auto writer = conn->CreatePacketWriter(Game::Message::SAuthSuccess);
			writer.WriteInteger(0, 8); // dummyThatNeedsToGoAway ?
			conn->SendPacket(writer);

			writer = conn->CreatePacketWriter(Game::Message::SRealmList);
			writer.WriteInteger<uint32_t>(1); // count
			writer.WriteInteger<uint32_t>(1); // id
			writer.WriteString(std::wstring(L"NexusEmu"));
			writer.WriteInteger<uint32_t>(0); // flags
			writer.WriteInteger<uint32_t>(4, 3); // status
			conn->SendPacket(writer);
		}

		void HandleSelectRealm(AuthConnection::Pointer& conn, Utils::BitStreamReader& reader) {
			conn->UnregisterMessageHandler(Game::Message::CSelectRealm);

			auto writer = conn->CreatePacketWriter(Game::Message::SGatewayInfo);
			writer.WriteInteger<uint32_t>(0x7F000001); // 127.0.0.1
			writer.WriteInteger<uint16_t>(24000); // port
			for (int i = 0; i < 16; i++)
				writer.WriteInteger<uint8_t>(255);
			writer.WriteInteger<uint32_t>(440369); // account id
			conn->SendPacket(writer);
		}

	private:
		std::wstring m_accountName;
	};

}
