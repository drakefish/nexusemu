#pragma once

namespace NexusEmu {
namespace Utils {

	class BitStreamReader {
	public:
		BitStreamReader() {}
		BitStreamReader(std::vector<uint8_t>& buffer) :
			m_buffer(buffer) {}

		std::vector<uint8_t>& getBuffer() {
			return m_buffer;
		}

		template<typename T>
		T ReadInteger(uint32_t bits = sizeof(T)*8) {
			return InternalRead<T>(bits);
		}
		
		template<>
		bool ReadInteger<bool>(uint32_t bits) {
			return InternalRead<uint32_t>(bits) != 0;
		}
		
		float ReadFloat() {
			uint32_t i = InternalRead<uint32_t>(sizeof(float)*8);
			return *reinterpret_cast<float*>(&i);
		}

		std::wstring ReadString() {
			bool b = ReadInteger<bool>(1);
			uint32_t count = ReadInteger<uint32_t>(b ? 15 : 7);
			std::wstring str;
			str.reserve(count);
			for (uint32_t i = 0; i < count; i++) {
				str.push_back(ReadInteger<wchar_t>(16));
			}
			return str;
		}

		std::string ReadAnsiString() {
			bool b = ReadInteger<bool>(1);
			uint32_t count = ReadInteger<uint32_t>(b ? 15 : 7);
			std::string str;
			str.reserve(count);
			for (uint32_t i = 0; i < count; i++) {
				str.push_back(ReadInteger<char>(8));
			}
			return str;
		}

		void Clear() {
			m_buffer.clear();
			m_bitsInPartialByte = 0;
			m_partialByte = 0;
			m_rpos = 0;
		}

	private:
		template<typename T>
		T InternalRead(uint32_t bits) {
			static_assert(std::numeric_limits<T>::is_integer, "T must be an integer.");

			if (bits > sizeof(T) * BYTE_SIZE) {
				throw std::exception("Requested number of bits exceeds the type's bitsize.");
			}

			T result = 0;
			uint32_t acquiredBits = 0;
			uint32_t v17 = 0;
			while (true) {
				// if partial byte is empty, cache one more.
				if (m_bitsInPartialByte == 0) {
					CacheByte();
				}

				uint32_t remainingBits = BYTE_SIZE - m_bitsInPartialByte;
				if (remainingBits > bits - acquiredBits) {
					remainingBits = bits - acquiredBits;
				}

				//result |= (T)((((1 << remainingBits) - 1) & (m_partialByte >> m_bitsInPartialByte)) << v17);
				result |= ((((1 << remainingBits) - 1) & (m_partialByte >> m_bitsInPartialByte)) << v17);
				acquiredBits = remainingBits + v17;
				v17 += remainingBits;

				m_bitsInPartialByte = (remainingBits + m_bitsInPartialByte) & 7;

				if (acquiredBits >= bits) {
					return result;
				}
			}
		}

		// Read and cache the next byte from the underlying buffer.
		void CacheByte() {
			if (m_rpos >= m_buffer.size()) {
				throw std::exception("Attempt to cache beyond the end of the BitStream.");
			}

			m_bitsInPartialByte = 0;
			m_partialByte = m_buffer[m_rpos];
			m_rpos++;
		}

		// Cache the next byte. If current partial byte is full this has no effect.
		void CacheNextByte() {
			if (m_bitsInPartialByte != 0)
				CacheByte();
		}

	private:
		static const int BYTE_SIZE = 8;
		std::vector<uint8_t> m_buffer;
		uint32_t m_rpos;
		uint8_t m_partialByte;
		int32_t m_bitsInPartialByte;
	};

}
}