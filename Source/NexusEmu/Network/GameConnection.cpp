#include "stdafx.h"
#include "GameConnection.h"

#define PACKET_SIZE_BITS 0x18
#define PACKET_SIZE_BYTES (PACKET_SIZE_BITS + 7) / 8
#define PACKET_ID_BITS 0xB // has changed from 0xB to 0xC over time
#define PACKET_ID_BYTES (PACKET_ID_BITS + 7) / 8
#define PACKET_HEADER_BITS PACKET_SIZE_BITS + PACKET_ID_BITS
#define PACKET_HEADER_BYTES (PACKET_HEADER_BITS + 7) / 8

namespace NexusEmu {
namespace Network {

	Utils::BitStreamWriter GameConnection::CreatePacketWriter(Game::Message id) {
		Utils::BitStreamWriter writer;
		writer.WriteInteger(0, PACKET_SIZE_BITS); // placeholder size.
		writer.WriteInteger(static_cast<uint32_t>(id), PACKET_ID_BITS);
		return writer;
	}

	void GameConnection::SendPacket(Utils::BitStreamWriter& writer) {
		// rewrite size (0x18 first bits)
		uint32_t* sizePtr = reinterpret_cast<uint32_t*>(writer.getBuffer().data());
		*sizePtr |= 0xFFFFFF & writer.getBuffer().size();
		writer.FlushByte();
		AsyncSend(writer.getBuffer());
	}

	void GameConnection::AsyncReadHeader() {
		/*m_buffer.resize(PACKET_HEADER_BYTES);
		boost::asio::async_read(getSocket(), boost::asio::buffer(m_buffer),
			std::bind(&GameConnection::HandleReadHeader, getThis<GameConnection>(),
			std::placeholders::_1, std::placeholders::_2));*/
		m_reader.Clear();
		m_reader.getBuffer().resize(PACKET_HEADER_BYTES);
		boost::asio::async_read(getSocket(), boost::asio::buffer(m_reader.getBuffer()),
			std::bind(&GameConnection::HandleReadHeader, getThis<GameConnection>(),
			std::placeholders::_1, std::placeholders::_2));
	}

	void GameConnection::HandleReadHeader(const boost::system::error_code& error, size_t bytes_transferred) {
		if (error) {
			if (error == boost::asio::error::eof) {
				std::cerr << "Client " << getSocket().remote_endpoint().address() << " disconnected (eof)." << std::endl;
				return Disconnect();
			}
			else {
				std::cerr << "HandleReadHeader() error: " << error.message() << std::endl;
				return;
			}
		}

		uint32_t size = m_reader.ReadInteger<uint32_t>(PACKET_SIZE_BITS);
		Game::Message id = static_cast<Game::Message>(m_reader.ReadInteger<uint32_t>(PACKET_ID_BITS));

		m_reader.getBuffer().resize(size);
		boost::asio::async_read(getSocket(),
			boost::asio::buffer(m_reader.getBuffer().data() + PACKET_HEADER_BYTES, size - PACKET_HEADER_BYTES),
			std::bind(&GameConnection::HandleReadBody, getThis<GameConnection>(),
			id, std::placeholders::_1, std::placeholders::_2));

		/*Utils::BitStreamReader reader(m_buffer);
		uint32_t size = reader.ReadInteger<uint32_t>(PACKET_SIZE_BITS);
		Game::Message id = static_cast<Game::Message>(reader.ReadInteger<uint32_t>(PACKET_ID_BITS));

		m_buffer.resize(size);
		boost::asio::async_read(getSocket(), 
			boost::asio::buffer(m_buffer.data() + PACKET_HEADER_BYTES, m_buffer.size() - PACKET_HEADER_BYTES),
			std::bind(&GameConnection::HandleReadBody, getThis<GameConnection>(),
			id, std::move(reader), std::placeholders::_1, std::placeholders::_2));*/
	}

	void GameConnection::HandleReadBody(Game::Message msgid, const boost::system::error_code& error, size_t bytes_transferred) {
		if (error) {
			if (error == boost::asio::error::eof) {
				std::cerr << "Client " << getSocket().remote_endpoint().address() << " disconnected (eof)." << std::endl;
				return Disconnect();
			}
			else {
				std::cerr << "HandleReadBody() error: " << error.message() << std::endl;
				return;
			}
		}

		HandleReceivedMessage(msgid, m_reader);
		AsyncReadHeader();
	}
}
}