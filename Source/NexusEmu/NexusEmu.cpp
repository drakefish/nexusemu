#include "stdafx.h"
#include <boost\asio.hpp>
#include <boost\thread.hpp>
#include <boost\bind.hpp>
#include <thread>
#include <functional>

#include "AuthServer\AuthServer.h"
#include "RealmServer\RealmServer.h"

int _tmain(int argc, _TCHAR* argv[])
{
	boost::asio::io_service iosvc;

	NexusEmu::AuthServer authServer(iosvc, tcp::endpoint(tcp::v4(), 23115));
	authServer.Start();

	NexusEmu::RealmServer realmServer(iosvc, tcp::endpoint(tcp::v4(), 24000));
	realmServer.Start();

	boost::thread thread(boost::bind(&boost::asio::io_service::run, &iosvc));

	std::cout << "Server started." << std::endl;
	std::string request;
	std::getline(std::cin, request);

	iosvc.stop();

	return 0;
}

